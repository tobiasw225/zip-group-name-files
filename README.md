# Zip Files with Group Name #


Zip files with a given group name: Given a group name, this program cycles the file system and creates a zip-archive with the files belonging to the users of the group.

You will need to run the program as su, since by default all files starting at "/" are included.


### Build from Sources

Clone the repository and build the program from source:

	$ git clone https://tobiasw225@bitbucket.org/tobiasw225/zip-group-name-files.git
	$ cd zip-group-name-files
	$ sudo dpkg -b zip_group_name_files_1.0-1
	$ sudo dpkg -i zip_group_name_files_1.0-1.deb

### Start the program

Run the program to zip all files with group name 'tobias' to tobias.zip in execution folder:

	$ sudo zip_group_name_files.py 'tobias'

You can also specify the name of the output file or run the script on a given folder. In this case I'm zipping all files in the home directory of the user tobias into test.zip


	$ sudo zip_group_name_files.py 'tobias' --output_file "/home/tobias/test.zip" --root_dir="/home/tobias/"
	
To change the default log file location, add the parameter 'log_file':

	$ sudo zip_group_name_files.py 'tobias' --output_file "/home/tobias/test.zip" --root_dir="/home/tobias/" --log_file="home/tobias/zip_group_files.log"


### Contact

Best contact me via email: tobiasw225@gmail.com

