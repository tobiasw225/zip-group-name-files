#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Zip Files with Group Name
#
# Implementation of a Python script for archiving files on a Linux system:
# A Python script should be implemented which locates all files owned
# by the members of a group and moved them to an archive folder.
# The name of the group should be a parameter of the program.
#
# The program should be robust, e.g. with respect to multiple invocations in short time.
# The events and results should be available in a log file.
# The program should be installable as a Debian package and should be made available for download.
#
# The Debian package does not have to be built, the sources are sufficient.
#
#
# Created by Tobias Wenzel in October 2019
# Copyright (c) 2019 Tobias Wenzel
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import argparse
import os
import re
import logging
import logging.config
from pathlib import Path
from zipfile import ZipFile
from datetime import datetime


__author__ = "Tobias Wenzel"
__copyright__ = "Copyright 2019, Wenzel"
__license__ = "MIT"
__version__ = "1.0.0"
__email__ = "tobiasw225@gmail.com"
__status__ = "Prototype"


def get_logger(logger_file: str,
               log_level=logging.INFO) -> logging.Logger:
    """
        helper function to adjust format of logger.


    :param logger_file:
    :param log_level:
    :return:
    """
    assert is_valid_path(logger_file)
    log_format = '%(asctime)s %(message)s'
    logger = logging.getLogger(__name__)
    logger.setLevel(log_level)

    handler = logging.handlers.RotatingFileHandler(logger_file)
    handler.setLevel(logging.DEBUG)

    formatter = logging.Formatter(log_format)
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    logging.basicConfig(level=log_level,
                        format=log_format)

    return logger


def is_valid_path(filename) -> bool:
    """
        Check if file is valid or existing.

    :param filename:
    :return:
    """
    if os.path.exists(filename) \
            or os.access(os.path.dirname(filename), os.W_OK):
        return True
    else:
        return False


def prev_execution(logger_file: str):
    """
        read the last time  a file was added
        to the archive with the same group-name.

    :param logger_file:
    :return:
    """
    with open(logger_file, 'r') as fin:
        text = fin.read()
    prev_times = re.findall(r'\d+-\d+-\d+ \d+:\d+:\d+', text)
    prev_groups = re.findall(r'group ([a-zA-Z]+) ', text)
    if not prev_times:
        # hasn't been started yet
        return 0, ''
    else:
        last_time = prev_times[-1]
        last_group = prev_groups[-1]
        time_diff = datetime.now() - datetime.strptime(last_time, '%Y-%m-%d %H:%M:%S')
        hours_since_last_zip = time_diff.seconds/(60*60)
        return hours_since_last_zip, last_group


def files_of_folder(folder: str, group: str) -> list:
    """

    :param folder:
    :param group:
    :return: all files in folder with given group
    """
    for filename in Path(folder).glob('**/*'):
        try:
            if Path.is_file(filename)\
                    and filename.group() == group:
                yield filename
        except KeyError:
            # if no group name is present skip the file.
            pass


def zip_files_of_group(group_name: str,
                       output_file: str,
                       root: str,
                       log_file: str):
    """

    :param group_name:
    :param output_file:
    :param root:
    :param log_file:
    :return:
    """

    if not os.path.isdir(root):
        print(f'{root} is no valid directory.')
        return

    if not output_file:
        # default output is the group_name.zip
        output_file = os.path.join(os.getcwd(), f"{group_name}_files.zip")

    if not is_valid_path(output_file) or os.path.isdir(output_file):
        print('Please specify a valid output filename'
              f' or change the user to get access to {output_file}')
        return
    if not is_valid_path(log_file):
        print('log file is not valid.')

    logger = get_logger(log_file)
    last_time_zipped, last_group_name = prev_execution(log_file)
    if last_time_zipped and last_group_name == group_name:
        ans = input(f'An archive was created {last_time_zipped:.2f}h ago. for the group {last_group_name}.\n'
                    'Do you want to overwrite it y or skip [n]?') or 'n'
        if ans.lower() == 'n':
            print('Exit script and keep old script.')
            return

    logger.info(f"Start to read from {root_dir} and zip all files of group {group_name} to {output_file}\n")
    i = 0
    error_count = 0
    with ZipFile(output_file, "w") as zip:
        for file in files_of_folder(root, group_name):
            if file == output_file:
                continue
            logger.info(file)
            try:
                zip.write(file)
            except ValueError as ve:
                error_count += 1
                logger.error(ve, file)
            except PermissionError as pe:
                error_count += 1
                logger.error(pe, file)
            except OSError as oe:
                error_count += 1
                logger.error(oe, file)
            i += 1

    logger.info(f"\nZipped {i} files into {output_file}.")
    logger.info(f"{error_count} files were skipped because of errors.")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("group", type=str,
                        help="Zip all files with group this group name.")
    parser.add_argument("--output_file", help="Filename of zip-archive.")
    parser.add_argument("--root_dir", help="Use a different folder than the default '/'.")
    parser.add_argument("--log_file", help="Use a custom log-file location. Default is stored  '/'.")
    args = parser.parse_args()
    root_dir = args.root_dir or '/'
    log_file = args.log_file or '/var/log/zip_group_name_files.log'
    zip_files_of_group(group_name=args.group,
                       output_file=args.output_file,
                       root=root_dir,
                       log_file=log_file)
